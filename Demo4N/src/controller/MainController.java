/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import model.person;
import views.MainFrame;

/**
 *
 * @author Usuario
 */
public class MainController implements ActionListener,FocusListener{
    MainFrame mainframe;
    JComboBox dComboBox;
    JComboBox mComboBox;
    JFileChooser d;
    person person;
    String[] departaments = new String[] { 
        "Boaco","Carazo","Chinandega","Chontales",
        "Costa Caribe Norte","Costa Caribe Sur",
        "Estelí","Granada","Jinotega","León",
        "Madriz","Managua","Masaya","Matagalpa",
        "Nueva Segovia","Río San Juan","Rivas" };
    String[] masayaMunicipalities = new String[] { 
        "Masaya","Nindirí", "Catarina", "Masatepe", 
        "La concepción","Niquinomo","Tisma","Nandasmo", 
        "San Juan de Oriente" };
    String[] managuaMunicipalities = new String[] { 
        "Managua","Ciudad Sandino", "El crucero", 
        "Mateare", "San Francisco Libre", 
        "San Rafael del Sur","Ticuantepe", "Tipitapa","Villa Carlos Fonseca" };
    String [] boacoMunicipalities = new String[] {
    "Boaco","Camoapa","San Jose de los remates","San lorenzo",
    "Santa Lucia","Tesutepe"};
    String [] carazoMunicipalities = new String[]{
        "Jinotepe","Diriamba","Dolores","El rosario",
        "La conquista","La paz","San Marcos","Santa Teresa"};
    String [] chinandegaMunicipalities = new String[]{
        "Chinandega","Chichigalpa","Corinto","El realejo","El viejo",
        "Posoltega","Puerto Morazan","San Francisco del Norte",
        "San Juan de Cinco Pinos","Santo Tomas del Norte",
        "Somotillo","Villanueva"};
    String[]chontalesmunicipalities = new String []{
      "Juigalpa","Acoyapa","Comalapa","El Coral",
        "La Libertad","San Francisco de Cuapa",
        "San Pedro de Lovago","Santo Domingo","Santo Tomas","Villa Sandino"};
    String [] CCNMunicipalitities = new String[]{
      "Puerto Cabezas","Bonanza","Mulukuku","Prinzapolka","Rosita",
        "Siuna","Waslala","Waspan"};
    String [] esteliMunicipalities = new String[]{
      "Esteli","Condega","La Trinidad","Pueblo Nuevp","San Juan de Limay",
      "San Nicolas"};
    String [] CCSMunicipalities = new String[]{
        "Blufields","Corn Island","Desembocadura de Rio Grande","El Ayote",
        "El rama","El Tortuguero","Kukra Hill","La Cruz de Rio Grande",
        "Laguna de Perlas","Muelle de Bueyes","Nueva Guinea","Paiwas"};
    String[] granadaMunicipalities = new String[] { 
        "Granada","Diriomo", "Diriá", "Nandaime"};
    String[] leonMunicipalities = new String[] { 
        "León","Achuapa", "El Jicaral", 
        "La Paz Centro","Larreynaga",
        "Nagarote","Quezalguaque"};
    String []jinotegamunicipalities = new String[]{
        "Jinotega","El Cua","La Concordia","San Jose de Bocay",
        "San Rafael del Norte","San Sebastian de Yali","Santa Maria De pantasma",
        "Wiwili"};
    String[]madrizmunicipalities = new String[]{
        "Somoto","Las Sabanas","Palacaguina","San Jose de Cusmapa",
        "San Juan de Rio Coco","San Lucas","Telpaneca","Totogalpa","Yalaguina"};
    String [] matagalpamunicipalities = new String[]{
        "Matagalpa","Ciudad Dario","El Tuma - La Dalia","Esquipulas",
        "Matiguas","Muy Muy","Rancho Grande","Rio Blanco","San Dionisio",
        "San Isidro","San Ramon","Sebaco"};
    String [] nuevasegoviamunicipalities = new String[]{
        "Ocotal","Ciudad Antigua","Dipilto","El Jicaro","Jalapa","Macuelizo",
        "Mozonte","Murra","Quilali","San Fernando","Santa Maria","Wiwili"};
    String [] riosanjuanmunicipalities = new String[]{
        "San Carlos","El Almendro","El Castillo","Morrito",
        "San Juan del Norte","San Miguelito"};
    String [] rivasMunicipalities = new String[] { 
        "Rivas","Altagracia", "Belén", "Buenos Aires",
        "Cárdenas","Moyogalpa","Potosí", "San Jorge"};
    DefaultComboBoxModel departamentsComboBoxModel = new DefaultComboBoxModel<>(departaments);
    
    public MainController(MainFrame f ){
        super();
        mainframe = f;
        d = new JFileChooser();
        person = new person();
    }

    public void setPerson(person b){
        person = b;
    }
    public void setPerson(String filePath){
        File f = new File(filePath);
        readperson(f);
    }
    public person getPerson(){
        return person;
    }     
    public void setDepartamentsComboBox(JComboBox jcombo){
        dComboBox = jcombo;
        dComboBox.setModel(departamentsComboBoxModel);
    }
    public void setMunicipalityComboBox(JComboBox jcombo){
        mComboBox = jcombo;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().getClass().getName().equals(JComboBox.class.getName())){
             JComboBox cbo = (JComboBox)e.getSource();
            switch(cbo.getName()){
                case "departaments":
                    switch(cbo.getSelectedItem().toString()){
                       case "Managua":
                           mComboBox.setModel(new DefaultComboBoxModel(managuaMunicipalities));
                           break;
                        case"Boaco":
                           mComboBox.setModel(new DefaultComboBoxModel(boacoMunicipalities));
                           break;
                        case "Matagalpa":
                            mComboBox.setModel(new DefaultComboBoxModel(matagalpamunicipalities));
                           break;
                        case "Masaya":
                            mComboBox.setModel(new DefaultComboBoxModel(masayaMunicipalities));
                           break;
                        case "Jinotega":
                            mComboBox.setModel(new DefaultComboBoxModel(jinotegamunicipalities));
                           break;
                        case "Carazo":
                            mComboBox.setModel(new DefaultComboBoxModel(carazoMunicipalities));
                           break;
                        case "Chinandega":
                            mComboBox.setModel(new DefaultComboBoxModel(chinandegaMunicipalities));
                           break;
                        case "Chontales":
                            mComboBox.setModel(new DefaultComboBoxModel(chontalesmunicipalities));
                           break;
                        case "Costa Caribe Norte":
                            mComboBox.setModel(new DefaultComboBoxModel(CCNMunicipalitities));
                           break;
                        case "Costa Caribe Sur":
                            mComboBox.setModel(new DefaultComboBoxModel(CCSMunicipalities));
                           break;
                        case "Estelí":
                            mComboBox.setModel(new DefaultComboBoxModel(esteliMunicipalities));
                           break;
                        case "León":
                            mComboBox.setModel(new DefaultComboBoxModel(leonMunicipalities));
                           break;
                        case "Madriz":
                            mComboBox.setModel(new DefaultComboBoxModel(madrizmunicipalities));
                           break;
                        case "Granada":
                            mComboBox.setModel(new DefaultComboBoxModel(granadaMunicipalities));
                           break;
                        case "Nueva Segovia":
                            mComboBox.setModel(new DefaultComboBoxModel(nuevasegoviamunicipalities));
                           break;
                        case "Río San Juan":
                            mComboBox.setModel(new DefaultComboBoxModel(riosanjuanmunicipalities));
                           break;
                        case "Rivas":
                            mComboBox.setModel(new DefaultComboBoxModel(rivasMunicipalities));
                           break;
                        default:
                            mComboBox.setModel(new DefaultComboBoxModel(new String[]{}));
                            break; 
                   }
<<<<<<< HEAD
                 break;   

=======
           
                     break;
>>>>>>> a3f685087aed939842be4795b190fd4d9409d9d3
           }
                      
       }else
        {
        switch (e.getActionCommand()){
            case "save":
                d.showSaveDialog(mainframe); 
                person = mainframe.getPersonData();
                writePerson(d.getSelectedFile());
                break;
            case "select":                
                d.showOpenDialog(mainframe);  
                person = readperson(d.getSelectedFile());
                mainframe.setPersonData(person);
                break;
            case "clear":
<<<<<<< HEAD
                mainframe.clean();
=======
                mainframe.clear();
>>>>>>> a3f685087aed939842be4795b190fd4d9409d9d3
                break;

        }
        }
    }

      private void writePerson(File file){
         try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file)); 
            w.writeObject(getPerson());
            w.flush();
        } catch (IOException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
      
    private person readperson(File file) {
       try{
            ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(file));
            return (person)ois.readObject();
        }
        catch(FileNotFoundException e){
            JOptionPane.showMessageDialog(mainframe, e.getMessage(),mainframe.getTitle(),JOptionPane.WARNING_MESSAGE);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private person[] readpersonlist(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
        person[] persons;
        
            FileInputStream in = new FileInputStream(file);
            ObjectInputStream s = new ObjectInputStream(in);
            persons = (person[]) s.readObject();
      
        return persons;
    }
    
    @Override
     public void focusGained(FocusEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
    @Override
    public void focusLost(FocusEvent e) {
        if(e.getOppositeComponent().getClass().getName().endsWith("JTextField")){
            switch(((javax.swing.JTextField) e.getSource()).getName()){
                case "idTextField":
                    System.out.println("Evento capturarado desde: idTextField" + ", valor: " +((javax.swing.JTextField) e.getSource()).getText());
                    break;
                default:
                    System.out.println("Evento captudarado desde: " + ((javax.swing.JTextField) e.getSource()).getName());
                    break;
            }
        }
    }
}
